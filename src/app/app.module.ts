import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';

import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {AppRoutes} from './app.routes';
import {FormsModule} from '@angular/forms';

import {MoviesModule} from './movies/movies.module';
import {TvShowsModule} from './tv-shows/tv-shows.module';
import {CoreModule} from './core/core.module';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, HomeComponent, LoginComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes, {preloadingStrategy: PreloadAllModules}),
    CoreModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
