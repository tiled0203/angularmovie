import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TvShow } from '../models/tv-show.model';

@Injectable()
export class TvShowsService {
    constructor(private http: HttpClient) {}

    getTvShows() {
        return this.http.get<TvShow[]>(`${environment.apiUrl}/tvshows`);
    }

    getTvShow(id: number) {
        return this.http.get<TvShow>(`${environment.apiUrl}/tvshows/${id}`);
    }

    searchTvShows(title: string, online: boolean) {
        return this.http.get<TvShow[]>(`${environment.apiUrl}/tvshows/search`, {
            params: new HttpParams().set('title', title).set('online', online.toString()),
        });
    }

    addTvShow(onlineId: number) {
        const addObj = {
            apiId: onlineId,
        };

        return this.http.post<TvShow>(`${environment.apiUrl}/tvshows/watchlist`, addObj);
    }
}
