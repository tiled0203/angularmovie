import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TvShowsService } from '../tv-shows.service';
import { Router } from '@angular/router';
import { TvShow } from '../../models/tv-show.model';
import {Observable} from 'rxjs';


@Component({
    selector: 'media-app-tv-shows-add',
    templateUrl: './tv-shows-add.component.html',
    styleUrls: ['./tv-shows-add.component.scss'],
})
export class TvShowsAddComponent implements OnInit {
    searchResults$: Observable<TvShow[]>;
    constructor(private service: TvShowsService, private routerService: Router) {}

    ngOnInit() {}

    lookUpTvShow(title: string) {
        this.searchResults$ = this.service.searchTvShows(title, true);
    }

    addToCollection(event: TvShow) {
        this.service.addTvShow(event.onlineId).subscribe(result => this.routerService.navigate(['/tvshows', result.id]));
    }
}
