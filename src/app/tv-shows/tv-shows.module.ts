import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TvShowsListComponent } from './tv-shows-list/tv-shows-list.component';
import { TvShowsDetailsComponent } from './tv-shows-details/tv-shows-details.component';
import { TvShowsAddComponent } from './tv-shows-add/tv-shows-add.component';
import { TvShowsService } from './tv-shows.service';
import { SharedModule } from '../shared/shared.module';
import { TvShowsRoutingModule } from './tv-shows-routing.module';

@NgModule({
    imports: [SharedModule, TvShowsRoutingModule],
    declarations: [TvShowsListComponent, TvShowsDetailsComponent, TvShowsAddComponent],
    providers: [TvShowsService],
})
export class TvShowsModule {}
