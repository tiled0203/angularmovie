import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TvShowsService } from '../tv-shows.service';

import { TvShow } from '../../models/tv-show.model';
import {Observable} from 'rxjs';

@Component({
    selector: 'media-app-tv-shows-list',
    templateUrl: './tv-shows-list.component.html',
    styleUrls: ['./tv-shows-list.component.scss'],
})
export class TvShowsListComponent implements OnInit {
    tvShows$: Observable<TvShow[]>;
    constructor(private service: TvShowsService) {}

    ngOnInit() {
        this.tvShows$ = this.service.getTvShows();
    }
}
