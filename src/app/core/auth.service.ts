import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    private _loggedIn = false;

    private LOGINKEY = 'AUTHSTATUS';

    get loggedIn() {
        return this._loggedIn;
    }

    constructor() {
        this._loggedIn = !!sessionStorage.getItem(this.LOGINKEY);
    }

    login(username: string, password: string) {
        this._loggedIn = username === password;
        sessionStorage.setItem(this.LOGINKEY, 'true');
    }
}
