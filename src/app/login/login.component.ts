import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { Router } from '@angular/router';
import { LoginFormValue } from '../models/loginForm.model';

@Component({
    selector: 'media-app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    constructor(private authService: AuthService, private router: Router) {}

    ngOnInit() {}

    login(formValue: LoginFormValue) {
        this.authService.login(formValue.username, formValue.password);
        this.router.navigate(['/']);
    }
}
