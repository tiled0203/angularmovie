import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../models/movie.model';
import { TvShow } from '../../models/tv-show.model';

@Component({
    selector: 'media-app-media',
    templateUrl: './media.component.html',
    styleUrls: ['./media.component.scss'],
})
export class MediaComponent implements OnInit {
    @Input() media: Movie | TvShow;

    @Output() selected = new EventEmitter<Movie | TvShow>();
    constructor() {}

    ngOnInit() {}
}
