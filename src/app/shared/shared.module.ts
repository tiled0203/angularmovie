import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { MediaComponent } from './media/media.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [CommonModule],
    declarations: [SearchComponent, MediaComponent],
    exports: [SearchComponent, MediaComponent, FormsModule, RouterModule, CommonModule],
})
export class SharedModule {}
