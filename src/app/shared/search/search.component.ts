import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import {Subject} from 'rxjs';


@Component({
    selector: 'media-app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
    search$ = new Subject<string>();
    @Output() searched = new EventEmitter<string>();

    constructor() {}

    ngOnInit() {
        this.search$.pipe(debounceTime(400), filter(x => x.length > 2), distinctUntilChanged()).subscribe(x => this.searched.next(x));
    }
}
