import {Routes} from '@angular/router';
import {MoviesListComponent} from './movies/movies-list/movies-list.component';
import {MoviesDetailsComponent} from './movies/movies-details/movies-details.component';
import {MoviesAddComponent} from './movies/movies-add/movies-add.component';
import {TvShowsListComponent} from './tv-shows/tv-shows-list/tv-shows-list.component';
import {TvShowsAddComponent} from './tv-shows/tv-shows-add/tv-shows-add.component';
import {TvShowsDetailsComponent} from './tv-shows/tv-shows-details/tv-shows-details.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './core/auth.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  }, {
    path: 'movies',
    loadChildren: () => import('./movies/movies.module').then(m => m.MoviesModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'tvshows',
    loadChildren: () => import('./tv-shows/tv-shows.module').then(m => m.TvShowsModule),
    canActivate: [AuthGuard],
  },
  {path: '**', redirectTo: '', pathMatch: 'full'},
];
