import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';
import { MoviesAddComponent } from './movies-add/movies-add.component';
import { MoviesService } from './movies.service';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MoviesRoutingModule } from './movies-routing.module';

@NgModule({
    imports: [MoviesRoutingModule, SharedModule],
    declarations: [MoviesListComponent, MoviesDetailsComponent, MoviesAddComponent],
    providers: [MoviesService],
})
export class MoviesModule {}
