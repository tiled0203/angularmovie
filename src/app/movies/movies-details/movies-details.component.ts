import {Component, Input, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {Movie} from '../../models/movie.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'media-app-movies-details',
  templateUrl: './movies-details.component.html',
  styleUrls: ['./movies-details.component.scss'],
})
export class MoviesDetailsComponent implements OnInit {
  movie: Movie;
  addToPublic = true;

  constructor(private service: MoviesService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.service.getMovie(id).subscribe(movie => (this.movie = movie));
  }

  deleteMovie(movie: Movie) {
    this.service.deleteMovie(movie.id).subscribe(() => this.router.navigate(['/movies']));
  }


}
