import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {Movie} from '../../models/movie.model';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';


@Component({
  selector: 'media-app-movies-add',
  templateUrl: './movies-add.component.html',
  styleUrls: ['./movies-add.component.scss'],
})
export class MoviesAddComponent implements OnInit {
  searchResults$: Observable<Movie[]>;
  public = false;

  constructor(private service: MoviesService, private router: Router) {
  }

  ngOnInit() {
  }

  lookUpMovie(title: string) {
    this.searchResults$ = this.service.searchMovies(title, this.public);
  }

  addToCollection(event: Movie) {
    alert('movie added to collection');
    this.service.addMovie(event.onlineId, this.public)
      .subscribe(result => this.router.navigate(['/movies', result.id]));
  }
}
