import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';
import { Movie } from '../../models/movie.model';
import { Router } from '@angular/router';
import {Observable} from 'rxjs';


@Component({
    selector: 'media-app-movies-list',
    templateUrl: './movies-list.component.html',
    styleUrls: ['./movies-list.component.scss'],
})
export class MoviesListComponent implements OnInit {
    movies$: Observable<Movie[]>;

    onlyPrivate = true;

    constructor(private service: MoviesService, private router: Router) {}

    ngOnInit() {
        this.getMovies();
    }

    getMovies() {
        this.movies$ = this.service.getMovies(this.onlyPrivate);
    }

    showDetails(event: Movie) {
        this.router.navigate(['/movies', event.id]);
    }

    searchMovie(title: string) {
        this.movies$ = this.service.searchMovies(title, false);
    }
}
